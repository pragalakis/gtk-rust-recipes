# Click Counter

![click counter screenshot](./click-counter-screenshot.png)

## How to run?

`cargo check`: Check if the app compiles
`cargo run`: Run the app
`cargo build`: Build the app
