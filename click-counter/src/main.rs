use gtk::glib::{self, clone};
use std::cell::Cell;
use std::rc::Rc;
use gtk::prelude::*;
use gtk::{self, Application, ApplicationWindow, Button, Label };

fn main() {
    let app = Application::builder()
        .application_id("org.gtk-rs.example")
        .build();

    app.connect_activate(build_ui);
    app.run();
}

fn build_ui(application: &Application) {
    let number = 0;
    fn label_text(count: &i32) -> String {
        let text = "Count: ".to_owned() + &count.to_string();
        return text;
    }

    // UI Elements
    let label = Label::builder()
        .label(&label_text(&number))
        .margin_top(12)
        .margin_bottom(12)
        .margin_start(12)
        .margin_end(12)
        .build();

    let button_increase = Button::builder()
        .label("Increase")
        .margin_top(12)
        .margin_bottom(12)
        .margin_start(12)
        .margin_end(6)
        .build();

    let button_decrease= Button::builder()
        .label("Decrease")
        .margin_top(12)
        .margin_bottom(12)
        .margin_start(6)
        .margin_end(12)
        .build();


    // Click Events
    let number = Rc::new(Cell::new(0));
     button_increase.connect_clicked(clone!(
             @weak number,
             @weak label =>
        move |_| {
            number.set(number.get() + 1);
            label.set_label(&label_text(&number.get()))
    }));

    button_decrease.connect_clicked(clone!(
            @weak label =>
        move |_| {
            number.set(number.get() - 1);
            label.set_label(&label_text(&number.get()))
    }));


    // Add buttons and label to `gtk_grid`
    let gtk_grid = gtk::Grid::builder().build();
    gtk_grid.attach(&label,0, 0, 1, 1);
    gtk_grid.attach(&button_increase, 1, 0, 1, 1);
    gtk_grid.attach(&button_decrease, 2, 0, 1, 1);

    // header
    let header_bar = gtk::HeaderBar::builder().build();
    header_bar.shows_title_buttons();
    header_bar.set_decoration_layout(Some("menu:close"));

    // Create a window
    let window = ApplicationWindow::builder()
        .application(application)
        .title("Click counter")
        .modal(true)
        .default_width(200)
        .child(&gtk_grid)
        .build();
    
    window.set_titlebar(Some(&header_bar));
    window.present();
}
