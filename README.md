# GTK - RUST Recipes

GUI development with Rust and GTK 4

## Recipes

- [Click counter](./click-counter)

## Docs

- [gtk4 docs](https://docs.gtk.org/gtk4)
- [gtk4-rs book](https://gtk-rs.org/gtk4-rs/stable/latest/book/)
- [GNOME Human Interface Guidelines](https://developer.gnome.org/hig/index.html)
- [GNOME docs & tutorials](https://developer.gnome.org/documentation/index.html)
